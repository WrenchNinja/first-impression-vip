angular.module('starter.services', [])

.factory('Chats', ["$firebaseArray", "$firebaseObject",
  function($firebaseArray, $firebaseObject) {
  // Might use a resource here that returns a JSON array

  var ref = new Firebase("https://popping-inferno-9926.firebaseio.com/chats");
  var chats = $firebaseArray(ref);

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {

      var index = chats.indexOf(chat);
      var link = chats[index];
      var deeperLink = link.$id;
      var chat = new Firebase("https://popping-inferno-9926.firebaseio.com/chats/" + deeperLink);

      var obj = $firebaseObject(chat);
      obj.$remove().then(function(chat) {
      }, function(error) {
        console.log("Error:", error);
      });

      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
}])

.factory('Highscores', ['$firebaseArray', '$firebaseObject', function($firebaseArray, $firebaseObject){

  var fireScores = new Firebase("https://popping-inferno-9926.firebaseio.com/highscores");
  var scores = $firebaseArray(fireScores);

  return {
    all: function(){
      return scores;
    },
    remove: function(score){

      var index = scores.indexOf(score);
      var link = scores[index];
      var deeperLink = link.$id;
      var score = new Firebase("https://popping-inferno-9926.firebaseio.com/highscores/" + deeperLink);

      var obj = $firebaseObject(score);
      obj.$remove().then(function(score) {
      }, function(error) {
        console.log("Error:", error);
      });

      scores.splice(scores.indexOf(score), 1);

    },
    addRoom: function(){
      var randomRoomId = Math.round(Math.random() * 100000000);
      var ref = new Firebase("https://popping-inferno-9926.firebaseio.com/test/" + randomRoomId);
      return $firebaseArray(ref);
    }
  };
}])