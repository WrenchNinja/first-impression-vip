angular.module('starter.controllers', [])

.controller('HighscoreCtrl', function($scope, $firebaseArray, Highscores){

  var highscoresFire = new Firebase("https://popping-inferno-9926.firebaseio.com/highscores");
  $scope.highscores = $firebaseArray(highscoresFire);

  $scope.shouldShowDelete = false;
  $scope.shouldShowReorder = false;
  $scope.listCanSwipe = true;

  $scope.addItem = function(){
    console.log("test");
    var name = prompt("What is your username?");
    var highscore = prompt("What is your highscore");
    if( name && highscore ){
      $scope.highscores.$add({
        "name": name,
        "score": highscore
      });
    }
  };

  $scope.scores = Highscores.all();

  $scope.removeScore = function(score){
    Highscores.remove(score);
    //console.log("delte");
  }

  $scope.addRoom = function(){
    
    Highscores.addRoom().$add({
      "test": "wtf"
    });

  }

})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
  popupService.showPopup("Authorization error", "Location services required to perform scanning");
})

.controller('BeaconCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaBeacon, $firebaseArray, $firebaseObject) {

  var timer;
  var startedCountdown = false;
  var beaconActive = false;
  var beaconStatusRef = new Firebase("https://popping-inferno-9926.firebaseio.com/beacons");
  var beaconStat = $firebaseObject(beaconStatusRef);

  //var name = prompt("Wat is je naam?");

  $("#username").change(function() {
    beaconStat.name = $("#username").val();
    beaconStat.$save().then(function(beaconStatusRef){
      beaconStatusRef.key() === beaconStat.$id;
    }, function(error){
      console.log("Error:", error);
    });
  });

  $("#color").change(function(){
    beaconStat.color = $( "#color" ).val();
    beaconStat.$save().then(function(beaconStatusRef){
      beaconStatusRef.key() === beaconStat.$id;
    }, function(error){
      console.log("Error:", error);
    });
  });
  
  
  

  function doTimeout(){
    //console.log('starting countdown', beaconActive, startedCountdown);

    if(startedCountdown == false && beaconActive == true){
      startedCountdown = true;
      timer = setTimeout(function(){
        $scope.currentMessage = "You aren't so close";
        console.log("not close");

        beaconStat.status = 0;
        beaconStat.name = "";
        beaconStat.color = "";
        beaconStat.$save().then(function(beaconStatusRef){
          beaconStatusRef.key() === beaconStat.$id;
        }, function(error){
          console.log("Error:", error);
        });

        beaconActive = false;
        $scope.$apply();
        startedCountdown = false;
      }, 2500);
    }

  }

  function stopTimeout(){
    console.log('clearing countdown', beaconActive);
    startedCountdown = false;
    clearTimeout(timer);
  }


  var handleBeacons = function(beacon, range, accuracy){
    if(range == "ProximityNear" || range == "ProximityImmediate" || accuracy < 2.2 ){
      stopTimeout();
      if($("#color").val() == "purple"){
        console.log($("#color").val());
        $("ion-header-bar").removeClass('green red blue yellow orange');
        $("ion-header-bar").addClass('purple');
      } else if($("#color").val() == "green" ){
        console.log($("#color").val());
        $("ion-header-bar").removeClass('purple red blue yellow orange');
        $("ion-header-bar").addClass('green');
      } else if($("#color").val() == "red" ){
        console.log($("#color").val());
        $("ion-header-bar").removeClass('green purple blue yellow orange');
        $("ion-header-bar").addClass('red');
      } else if($("#color").val() == "blue"){
        console.log($("#color").val());
        $("ion-header-bar").removeClass('green red purple yellow orange');
        $("ion-header-bar").addClass('blue');
      } else if($("#color").val() == "yellow"){
        console.log($("#color").val());
        $("ion-header-bar").removeClass('green red blue purple orange');
        $("ion-header-bar").addClass('yellow');
      } else if($("#color").val() == "orange"){
        console.log($("#color").val());
        $("ion-header-bar").removeClass('green red blue yellow purple');
        $("ion-header-bar").addClass('orange');
      }
      if(beaconActive == false){
        beaconActive = true;
        $scope.currentMessage = "You are close to the radio";

        beaconStat.status = 1;
        beaconStat.name = $("#username").val();
        beaconStat.color = $( "#color" ).val();
        beaconStat.$save().then(function(beaconStatusRef){
          beaconStatusRef.key() === beaconStat.$id;
        }, function(error){
          console.log("Error:", error);
        });

        $scope.$apply();
      }
    } else{
      $("ion-header-bar").removeClass('green red blue yellow orange purple');
      doTimeout();
    }
  }

  $scope.beacons = {};
  $scope.log = {};

  // Simulate Beacon ranges
  $(document).on("keypress", function(e){

    if(e.keyCode == 49){
      handleBeacons("Shoe", "ProximityNear");
    } else if(e.keyCode == 50){
      handleBeacons("Shoe", "ProximityFar");
    } else if(e.keyCode == 51){
      handleBeacons("Shoe", "ProximityUnknown");
    }

  });

  /* Cordova */
  $ionicPlatform.ready(function(){

    $cordovaBeacon.requestWhenInUseAuthorization();

    $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function(event, data) {
        var uniqueBeaconKey;
          
        

        //$scope.apply();
        for(var i = 0; i < data.beacons.length; i++) {
            uniqueBeaconKey = data.beacons[i].uuid + ":" + data.beacons[i].major + ":" + data.beacons[i].minor;
            $scope.beacons[uniqueBeaconKey] = data.beacons[i];
            var object = data;
            $scope.log = data;

            handleBeacons(data.beacons[i].uuid, data.beacons[i].proximity);
            /* Beacon monitoring * /
            if( data.beacons[i].uuid == "D0D3FA86-CA76-45EC-9BD9-6AF48832B1B5" && data.beacons[i].proximity == "ProximityNear" ){
              $scope.currentMessage = "Bike is close";
            }

            if( data.beacons[i].uuid == "D0D3FA86-CA76-45EC-9BD9-6AF48832B1B5" && data.beacons[i].proximity == "ProximityImmediate" ){
              $scope.currentMessage = "Bike is very close";
            }

            if( data.beacons[i].uuid == "D0D3FA86-CA76-45EC-9BD9-6AF46B25985B" && data.beacons[i].proximity == "ProximityNear" ){
              $scope.currentMessage = "Bag";
            }

            if( data.beacons[i].proximity == "ProximityNear" || data.beacons[i].proximity == "ProximityImmediate"){
              $scope.beaconActive = true;
              stopTimeout();
              $scope.currentMessage = "You are close to the radio";
            } else if( data.beacons[i].proximity != "ProximityNear" || data.beacons[i].proximity != "ProximityImmediate" ){
              if($scope.beaconActive = true){

                doTimeout();
                
              }
              
            }

            // master comment below 
            /**/
            

        }

        $scope.$apply();

    });

    $cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Ice", "B9407F30-F5F8-466E-AFF9-25556B57FE6D", 40260, 52233 ));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Mint", "B9407F30-F5F8-466E-AFF9-25556B57FE6D", 27818, 36461 ));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Blueberry", "B9407F30-F5F8-466E-AFF9-25556B57FE6D", 9779, 49595));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Bike", "D0D3FA86-CA76-45EC-9BD9-6AF48832B1B5" ));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Bag", "D0D3FA86-CA76-45EC-9BD9-6AF46B25985B"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Dog", "D0D3FA86-CA76-45EC-9BD9-6AF4A8BFEF41"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Bed", "D0D3FA86-CA76-45EC-9BD9-6AF498ABF996"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Shoe", "D0D3FA86-CA76-45EC-9BD9-6AF4F0AD6BAF"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Door", "D0D3FA86-CA76-45EC-9BD9-6AF4CBB287B6"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Chair", "D0D3FA86-CA76-45EC-9BD9-6AF434352713"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Generic", "D0D3FA86-CA76-45EC-9BD9-6AF45A8F6B8C"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Fridge", "D0D3FA86-CA76-45EC-9BD9-6AF4CB237B49"));
    //$cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Car", "D0D3FA86-CA76-45EC-9BD9-6AF46376DB7C"));

  });

  /**/

});
